function captchaSolved(token) {
  if (token) {
    showSuccess();
  } else {
    showFailure();
  }
}

function showSuccess() {
  const overlay = document.getElementById('overlay');
  const verifyingBox = document.querySelector('.challenge-box.verifying');
  const successBox = document.querySelector('.challenge-box.success');
  const failureBox = document.querySelector('.challenge-box.failure');
  
  verifyingBox.style.display = 'none';
  failureBox.style.display = 'none';
  successBox.style.display = 'flex';
  
  setTimeout(() => {
    overlay.classList.add('removing');
    setTimeout(() => {
      overlay.style.display = 'none';
    }, 300);
  }, 1000);
}

function showFailure() {
  const verifyingBox = document.querySelector('.challenge-box.verifying');
  const successBox = document.querySelector('.challenge-box.success');
  const failureBox = document.querySelector('.challenge-box.failure');
  
  verifyingBox.style.display = 'none';
  successBox.style.display = 'none';
  failureBox.style.display = 'flex';
  
  setTimeout(() => {
    location.reload();
  }, 2000);
}